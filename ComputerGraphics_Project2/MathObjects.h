//
//  MathObjects.h
//  ComputerGraphics_Project2
//
//  Created by Giulio Stramondo on 15/10/13.
//  Copyright (c) 2013 Giulio Stramondo. All rights reserved.
//

#ifndef ComputerGraphics_Project2_MathObjects_h
#define ComputerGraphics_Project2_MathObjects_h

#include <stdlib.h>


class Matrix{
public:

    Matrix(int size);
    Matrix(int sizeX, int sizeY);
    float get(int x,int y);
    void set_row(...);
    void set_row_id(int id, float a, float b, float c, float d);
    void set_item(int x, int y, float value);
    void print();
    Matrix operator*(const Matrix& other);
    Matrix operator+(const Matrix& other);
    void identity();
    void zeroInit();
    void transpose();
    bool squared;
    
private:
    float **items;
    int current_row;
    int size;
    int sizeX,sizeY;
};

class Vector: public Matrix{
public:
    Vector(int size);
    Vector operator^(const Vector& other);
    float operator*(const Vector& other);
    Vector normalize();
    float length();
    float get(int position);
    void set(int position, float value);
    void transpose();
    
private:
    int size;
    bool column;
};

#endif
