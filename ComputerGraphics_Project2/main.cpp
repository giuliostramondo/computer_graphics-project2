//
//  Computer_Graphics
//
//  Created by Giulio Stramondo on 12/09/13.
//  Copyright (c) 2013 Giulio Stramondo. All rights reserved.
//

#ifdef WIN32
#define WINDOWS
#elif WIN64
#define WINDOWS
#endif
#ifdef WINDOWS
#include <windows.h>  // Without include this header in Windows, you will get error C2381: 'exit' : redefinition; __declspec(noreturn) differs
#endif


#ifdef __APPLE__
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "DrawFunctions.h"
#include <list>
#include <iostream>
#include "Objects.h"
#include "MathObjects.h"
#include "reader.h"
#include "GraphicObjects.h"
#include <list>
#include "SceneObjects.h"



/* Intialization of the sign of drawing graphics */
int scene=0;
int offsetGx=0;
int offsetGy=0;

Scene s;

void toggleView(int i){
    if(i==0){
        s.vrc.vrp.x=50;
        s.vrc.vrp.y=20;
        s.vrc.vrp.z=20;
        
        s.vrc.vpn.x=0;
        s.vrc.vpn.y=0;
        s.vrc.vpn.z=1;
        
        s.vrc.vup.x=0;
        s.vrc.vup.y=1;
        s.vrc.vup.z=0;
    }
    else if(i==1){
        s.vrc.vrp.x=60;
        s.vrc.vrp.y=20;
        s.vrc.vrp.z=-5;
        
        s.vrc.vpn.x=1;
        s.vrc.vpn.y=0;
        s.vrc.vpn.z=0;
        
        s.vrc.vup.x=0;
        s.vrc.vup.y=1;
        s.vrc.vup.z=0;
    }
    else if(i==2){
        s.vrc.vrp.x=40;
        s.vrc.vrp.y=40;
        s.vrc.vrp.z=10;
        
        s.vrc.vpn.x=0;
        s.vrc.vpn.y=1;
        s.vrc.vpn.z=0;
        
        s.vrc.vup.x=1;
        s.vrc.vup.y=0;
        s.vrc.vup.z=0;
        
    }
}

//this function allows to move between the scenes
void Keyboard(unsigned char key, int x, int y)
{
	//cout<<key<<endl;

	switch (key)
	{
            
		case 27:       //ESCAPE key
			exit(0);
			break;
            
        case 't':
            scene++;
            if(scene==3)scene=0;
            toggleView(scene);
            
            break;
        case 'i':
            if(scene==0) s.vrc.vrp.z-=2;
            if(scene==1) s.vrc.vrp.x-=2;
            if(scene==2) s.vrc.vrp.y-=2;
            
            break;
            
        case 'o':
            if(scene==0) s.vrc.vrp.z+=2;
            if(scene==1) s.vrc.vrp.x+=2;
            if(scene==2) s.vrc.vrp.y+=2;
            
            break;
            
        case 'r':
            Vector V_vpn=*new Vector(4);
            V_vpn.set(0, s.vrc.vpn.x+s.vrc.vrp.x);
            V_vpn.set(1, s.vrc.vpn.y+s.vrc.vrp.y);
            V_vpn.set(2, s.vrc.vpn.z+s.vrc.vrp.z);
            V_vpn.set(3, 1);
            
            Vector V_vrp=*new Vector(4);
            V_vrp.set(0, s.vrc.vrp.x);
            V_vrp.set(1, s.vrc.vrp.y);
            V_vrp.set(2, s.vrc.vrp.z);
            V_vrp.set(3, 1);
            
            Matrix newVpn=Ry3D(2)*V_vpn;
            s.vrc.vpn.x=newVpn.get(0, 0)/newVpn.get(0, 3);
            s.vrc.vpn.y=newVpn.get(0, 1)/newVpn.get(0, 3);
            s.vrc.vpn.z=newVpn.get(0, 2)/newVpn.get(0, 3);
            
            Matrix newVrp=Ry3D(2)*V_vrp;
            s.vrc.vrp.x=newVrp.get(0, 0)/newVrp.get(0, 3);
            s.vrc.vrp.y=newVrp.get(0, 1)/newVrp.get(0, 3);
            s.vrc.vrp.z=newVrp.get(0, 2)/newVrp.get(0, 3);
            
            printf("\nVpn:\n");
            newVpn.print();
            printf("\nVrp:\n");
            newVrp.print();
            
                        s.vrc.vpn.x-=s.vrc.vrp.x;
                        s.vrc.vpn.y-=s.vrc.vrp.y;
                        s.vrc.vpn.z-=s.vrc.vrp.z;
            
            printf("\nVpn: moved\n");
            printf("%f\n%f\n%f\n",s.vrc.vpn.x,s.vrc.vpn.y,s.vrc.vpn.z);
            
            
            break;
            
	}
    
}

// this function was used during the making of each scene
// to find the optimal position of the objects

void SpecKeyboard(int key, int x, int y){
    switch (key)
	{
        case GLUT_KEY_UP:
            offsetGy+=5;
            break;
        case GLUT_KEY_DOWN:
            offsetGy-=5;
            break;
        case GLUT_KEY_RIGHT:
            offsetGx+=5;
            s.vrc.vrp.x+=5;
            break;
        case GLUT_KEY_LEFT:
            offsetGx-=5;
            s.vrc.vrp.x-=5;
            break;
    }
}





/******************************************************************/

/* output code by Mark Kilgard */
void output(int x, int y, char *string)
{
	int len, i;
	glRasterPos2f(x, y);
	len = (int) strlen(string);
	for (i = 0; i < len; i++)
	{
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_18, string[i]);
	}
}


/******************************************************************/





void display(void) {
	/* clear the screen to the clear colour */

	glClear(GL_COLOR_BUFFER_BIT);
	// draw a red square



        Node *sceneNode= new Node[s.nodeCount];
        Face *sceneFace= new Face[s.faceCount];
        
        Matrix N=Mper(-1)*Nper(s.vrc,s.conVol);
        EdgeList* bed_lamp=NULL;
        Edge3DList* bed_lamp3D=NULL;
        
        for(int i=0;i<s.nodeCount;i++){
            Vector tmp=*new Vector(4);

            tmp.set(0,s.nodeList[i].point.x);
            tmp.set(1,s.nodeList[i].point.y);
            tmp.set(2,s.nodeList[i].point.z);
            tmp.set(3,1);
            Matrix tmp1=Mvv3dv(s.conVol)*(N*tmp);
    
            sceneNode[i].point.x=tmp1.get(0, 0)/tmp1.get(0, 3);
            sceneNode[i].point.y=tmp1.get(0, 1)/tmp1.get(0, 3);
            sceneNode[i].point.z=tmp1.get(0, 2)/tmp1.get(0, 3);

            
        }
        Connectivity* tmp=s.nodeConnectionsHead;
       
        while(tmp!=NULL){
            Point2D ini=*new Point2D(sceneNode[tmp->id1].point.x,sceneNode[tmp->id1].point.y);
            Point2D fin=*new Point2D(sceneNode[tmp->id2].point.x,sceneNode[tmp->id2].point.y);
            
            Point3D ini3D=*new Point3D(s.nodeList[tmp->id1].point.x,s.nodeList[tmp->id1].point.y,s.nodeList[tmp->id1].point.z);
            Point3D fin3D=*new Point3D(s.nodeList[tmp->id2].point.x,s.nodeList[tmp->id2].point.y,s.nodeList[tmp->id2].point.z);
            
            Edge tmp1=*new Edge(ini,fin);
            Edge3D tmp3D=*new Edge3D(ini3D,fin3D);
            
            if(bed_lamp==NULL){
                bed_lamp=new EdgeList();
                bed_lamp->setEdge(tmp1);
            }
            else{
              bed_lamp->addEdge(tmp1);  
            }
            
            if(bed_lamp3D==NULL){
                bed_lamp3D=new Edge3DList(tmp3D);
            }
            else{
                bed_lamp3D->addEdge(tmp3D);
            }

            tmp=tmp->next;
        }

        EdgeList* bed_lamp_proj=projectEdge3DList(s.vrc, s.conVol, bed_lamp3D);
        bed_lamp_proj->setColor(1, 1, 0);
        DrawEdgeList(bed_lamp_proj);
        

        
        Edge3DList* cyli=DiscretizeCylinderBody(s.cylinder.center.x, s.cylinder.center.y, s.cylinder.center.z, false, s.cylinder.radius, 8, s.cylinder.height);

        DrawEdgeList(projectEdge3DList(s.vrc, s.conVol, cyli));

        
//        std::list <Point3D> sqpoints;
//        sqpoints.push_back(*new Point3D(0,0,10));
//        sqpoints.push_back(*new Point3D(0,20,10));
//        sqpoints.push_back(*new Point3D(20,20,10));
//        sqpoints.push_back(*new Point3D(20,0,10));
//        Edge3DList* square=getEdge3DListfromPointList(sqpoints);
//
//        EdgeList* squareProjected=projectEdge3DList(s.vrc, s.conVol, square);
//        squareProjected->print();
//        squareProjected->setColor(1, 0, 0);
//        squareProjected->print();
//        DrawEdgeList(squareProjected);
        
//        for(Edge3DList *tmp=list;tmp!=NULL;tmp=tmp->next()){
//            tmp->e.print();
//        }
//        Edge3DList* list=discretizeCircle1(10, 10, 10, false, 10,40);
//        DrawEdgeList(projectEdge3DList(s.vrc, s.conVol, list));

        
        
        for(int i=0;i<s.faceCount;i++){
            Vector tmp=*new Vector(4);
            
            tmp.set(0,s.faceList[i].point.x);
            tmp.set(1,s.faceList[i].point.y);
            tmp.set(2,s.faceList[i].point.z);
            tmp.set(3,1);
            Matrix tmp1=Mvv3dv(s.conVol)*(N*tmp);

            
            sceneFace[i].point.x=tmp1.get(0, 0)/tmp1.get(0, 3);
            sceneFace[i].point.y=tmp1.get(0, 1)/tmp1.get(0, 3);
            sceneFace[i].point.z=tmp1.get(0, 2)/tmp1.get(0, 3);
            
            
            
        }
        
        for(int i=0;i<s.faceCount-1;i++){
            DrawLine_colored(sceneFace[i].point.x, sceneFace[i].point.y, sceneFace[i+1].point.x, sceneFace[i+1].point.y,1,1,0);
        }
        DrawLine_colored(sceneFace[0].point.x, sceneFace[0].point.y, sceneFace[s.faceCount-1].point.x, sceneFace[s.faceCount-1].point.y,1,1,0);
	    
    /* swap buffers */
    glutSwapBuffers();
}

void reshape (int w, int h) {
	/* set the viewport */
	glViewport (0, 0, (GLsizei) w, (GLsizei) h);
    
	/* Matrix for projection transformation */
	glMatrixMode (GL_PROJECTION);
    
	/* replaces the current matrix with the identity matrix */
	glLoadIdentity ();
    
	/* Define a 2d orthographic projection matrix */
	gluOrtho2D (0.5, (GLdouble) w, 0.5, (GLdouble) h);
}

/*******************************************************************/



int main(int argc, char** argv) {
    const char * filename = "project2.xml";
    
    openFile(filename);

    parseDatabase();
    printDatabase();
    s=*loadScene();


	/* deal with any GLUT command Line options */
    glutInit(&argc, argv);
    
	/* create an output window */
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB);
	glutInitWindowSize(800, 800);
    
	/* set the name of the window and try to create it */
    glutCreateWindow("CS 488 - Sample");
    
	/* specify clear values for the color buffers */
	glClearColor (0.0, 0.0, 0.0, 1.0);
    
    /* Receive keyboard inputs */
    glutKeyboardFunc (Keyboard);
    
    /*Receive special keyboard inputs */
    glutSpecialFunc(SpecKeyboard);
    
    
    /* assign the display function */
    glutDisplayFunc(display);
    
	/* assign the idle function */
    glutIdleFunc(display);
    
    /* sets the reshape callback for the current window */
	glutReshapeFunc(reshape);
    
    /* enters the GLUT event processing loop */
    glutMainLoop();
    
    
    return (EXIT_SUCCESS);
}
