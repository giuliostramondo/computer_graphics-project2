//
//  Objects.h
//  Computer_Graphics
//
//  Created by Giulio Stramondo on 23/09/13.
//  Copyright (c) 2013 Giulio Stramondo. All rights reserved.
//

#ifndef Computer_Graphics_Objects_h
#define Computer_Graphics_Objects_h
#include <list>


class Point2D{
public:
    Point2D(int x,int y);
    int x;
    int y;
};

class Point3D{
public:
    Point3D(float x,float y,float z);
    float x;
    float y;
    float z;
};


class Color1{
public:
    Color1(float r,float g,float b);
    float r;
    float g;
    float b;
};

class Edge{
public:
    Edge(Point2D ini,Point2D fin);
    Edge();
    Edge(int x1,int y1, int x2,int y2,float r,float g,float b);
    Edge(int x1,int y1, int x2,int y2);
    void setColor(float r,float g,float b);
    void print();
    Point2D ini;
    Point2D fin;
    Color1 color;
};

class Edge3D{
public:
    Edge3D();
    Edge3D(Point3D i,Point3D f);
    void setColor(float r,float g,float b);
    void print();
    Point3D ini;
    Point3D fin;
    Color1 color;
    
};
class EdgeList{
public:
    EdgeList();
    EdgeList(Edge e);
    Edge e;
    void setEdge(int x1,int y1, int x2,int y2,float r,float g,float b);
    void setEdge(Edge e);
    void addEdge(Edge e);
    void print();
    void setColor(float r,float g,float b);
    EdgeList* next();
    EdgeList* head();
    
private:
    EdgeList *succ;
    EdgeList *h;
};

class Edge3DList{
public:
    Edge3DList();
    Edge3DList(Edge3D e);
    Edge3D e;
    void setEdge(float x1,float y1,float z1, float x2,float y2,float z2, float r,float g,float b);
    void addEdge(Edge3D e);
    void merge(Edge3DList* toAppend);
    Edge3DList* next();
    Edge3DList* head();
    void print();
private:
    Edge3DList *succ;
    Edge3DList *h;

};







#endif
