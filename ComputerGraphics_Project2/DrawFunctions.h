//
//  DrawFunctions.h
//  Computer_Graphics
//
//  Created by Giulio Stramondo on 12/09/13.
//  Copyright (c) 2013 Giulio Stramondo. All rights reserved.
//

#ifndef Computer_Graphics_DrawFunctions_h
#define Computer_Graphics_DrawFunctions_h

#include "Objects.h"
#include "reader.h"
#include "GraphicObjects.h"
#include <list>
#include <math.h>
#include <GLUT/glut.h>


//Data structure used to implement the active edge algorithm-> this is a row element of the Edge table
struct edgeTableEntry{
    float ymax;
    float xmin;
    float slope;
    struct edgeTableEntry* next;
};

//Data structure used to implement the active edge algorithm-> this is the Edge table
struct edgeTable{
    struct edgeTable *up;
    int ymin;
    struct edgeTableEntry* right;
};


//This function adds one entry in the edgeTable:
//Initialize the ET if it's empty ( head == NULL )
//Locate the column on the ET where it's needed to add the edge ( create it if the column with the corrispondent ymin doesn't exist yet)
//go until the ends of the row and add the new edge
edgeTable* addEdge(Point2D ymin, Point2D p1,edgeTable* head){
    edgeTable* localPtr=NULL;
    edgeTable* oldValue=NULL;
    
    if (head == NULL){
        head=(edgeTable*) malloc(sizeof(edgeTable));
        head->ymin=ymin.y;
        head->up=NULL;
        head->right=NULL;
    }
    
    localPtr=head;
    while( localPtr!=NULL && localPtr->ymin!=ymin.y){
        oldValue=localPtr;
        localPtr=localPtr->up;
    }
    
    if(localPtr==NULL){
        oldValue->up=(edgeTable*)malloc(sizeof(edgeTable));
        oldValue->up->ymin=ymin.y;
        oldValue->up->up=NULL;
        oldValue->up->right=NULL;
        localPtr=oldValue->up;
    }
    edgeTableEntry* entryPointer;
    edgeTableEntry* oldPointer;
    entryPointer=localPtr->right;
    
    if(entryPointer==NULL){
        localPtr->right=(edgeTableEntry*)malloc(sizeof(edgeTableEntry));
        entryPointer=localPtr->right;
        
        entryPointer->ymax=p1.y;
        /* if(ymin.x<p1.x)
         entryPointer->xmin=ymin.x;
         else
         entryPointer->xmin=p1.x;*/
        
        entryPointer->xmin=ymin.x;
        entryPointer->slope=((float)ymin.x-p1.x)/((float)ymin.y-p1.y);
        
        entryPointer->next=NULL;
        
        return head;
    }
    while (entryPointer!=NULL){ oldPointer=entryPointer; entryPointer=entryPointer->next;}
    
    oldPointer->next=(edgeTableEntry*)malloc(sizeof(edgeTableEntry));
    
    entryPointer=oldPointer->next;
    
    entryPointer->ymax=p1.y;
    /* if(ymin.x<p1.x)
     entryPointer->xmin=ymin.x;
     else
     entryPointer->xmin=p1.x;*/
    entryPointer->xmin=ymin.x;
    
    entryPointer->slope=((float)ymin.x-p1.x)/((float)ymin.y-p1.y);
    
    entryPointer->next=NULL;
    
    return head;
    
}

//This function takes in input the list of points of the polygon
//and outputs the edgeTable builded on the list of points

edgeTable* createEdgeTable(std::list<Point2D>points){
    edgeTable* head=NULL;
    
    int flag=1;
    int yminold=-1;
    
    while (flag){
        
        int ymin=-1;
        
        Point2D *prev;
        Point2D *next;
        Point2D *min;
        std::list<Point2D>::iterator minIt;
        
        
        flag=0;
        for(std::list<Point2D>::iterator it=points.begin(); it!=points.end();it++){
            if((ymin == -1 || it->y<ymin)&&it->y>yminold){
                ymin=it->y;
                flag=1;
            }
        }
        
        
        for(std::list<Point2D>::iterator it=points.begin(); it!=points.end() && flag==1;it++){
            if(it->y==ymin){
                min=&*it;
                if(min==&points.front())prev=&points.back();
                else{
                    it--;
                    prev=&*it;
                    it++;
                }
                if(min==&points.back())next=&points.front();
                else{
                    it++;
                    next=&*it;
                    it--;
                }
                if(prev->y>min->y){
                    head=addEdge(*min, *prev, head);
                }
                if(next->y>min->y){
                    head=addEdge(*min, *next, head);
                }
                
                
            }
        }
        yminold=ymin;
        
    }
    return head;
}

//This function had been used just for debugging purpose
//prints the complete Edge table in a human readable way
void printEdgeTable(edgeTable* head){
    edgeTableEntry*entry=NULL;
    while(head!=NULL){
        printf("ymin: %d ->",head->ymin);
        entry=head->right;
        while(entry!=NULL){
            printf("|ymax: %f|xmin: %f|slope: %f|->",entry->ymax,entry->xmin,entry->slope);
            entry=entry->next;
        }
        printf("NULL\n");
        head=head->up;
    }
}

//This function fills the row y from startX column to end X column with color RGB
void fill(int y,float startX, float endX,float R,float G,float B){
    glColor3f(R,G,B);
    glBegin(GL_POINTS);
    int x1=ceil(startX);
    int x2=floor(endX);
    for(int i=x1; i<=x2;i++){
        glVertex2f(i, y);
    }
    glEnd();
    glColor3f(1.0, 1.0, 1.0);
}

//This function is used by the active edge filling algorithm to calculate the next Scanline ( which corrisponds to scanline currentY )
edgeTableEntry* scanlineUpdate(edgeTableEntry* head,int currentY){
    edgeTableEntry* i=head;
    edgeTableEntry *prec=NULL;
    while(i!=NULL){
        if(i->ymax==currentY){
            if(prec!=NULL){
                prec->next=i->next;
                free(i);
                i=prec->next;
            }
            else{
                head=i->next;
                free(i);
                i=head;
            }
        }
        else{
            i->xmin=i->xmin+i->slope;
            prec=i;
            i=i->next;
        }
    }
    return head;
}

//this functions merges one row of the Edge table with the current scanline
//done when the scanline reaches the same ymin of the entry present in the edge table

edgeTableEntry* mergeScanline(edgeTableEntry* head, edgeTableEntry* toMerge){
    if(head==NULL)return toMerge;
    edgeTableEntry* it=head;
    while(it->next!=NULL)it=it->next;
    it->next=toMerge;
    return head;
}

//This function is used just for debugging purpose
//it prints the entry contained in the scanline
void printScanline(edgeTableEntry* entry){
    while(entry!=NULL){
        printf("|ymax: %f|xmin: %f|slope: %f|->",entry->ymax,entry->xmin,entry->slope);
        entry=entry->next;
    }
    printf("NULL\n");
}

//this function draw a filled polygon using the Active edge alghoritm
//takes as input the ordered list of points that are the polygon vertex
void drawFilledPoligon(std::list<Point2D>points,float r, float g, float b){
    edgeTable* head=createEdgeTable(points);
    edgeTable* ETiter=head;
    std::list<float>intersections;

    
    edgeTableEntry* ScanLine=head->right;
    edgeTableEntry*tmp=ScanLine;
    int y=head->ymin;
    ETiter=head->up;
    
    while(ScanLine!=NULL){

        tmp=ScanLine;
        while(tmp!=NULL){
            intersections.push_front(tmp->xmin);
            tmp=tmp->next;
        }
        intersections.sort();
        for(std::list<float>::iterator it=intersections.begin();it!=intersections.end();it++){
            float x1=*it;
            it++;
            float x2=*it;
            
            fill(y,x1,x2,r,g,b);
            
        }
        intersections.clear();
        y++;
        if(ETiter!=NULL&&y==ETiter->ymin){
            ScanLine=mergeScanline(ScanLine, ETiter->right);
            ETiter=ETiter->up;
        }
        ScanLine=scanlineUpdate(ScanLine, y);
    }

}
Point2D* projectPoint3D(VRC vrc,CononicalVolume canVol,Point3D point){
    Vector tmp=*new Vector(4);
    Matrix completeMatrix=(Mvv3dv(canVol)*(Mper(-1)*Nper(vrc, canVol)));
    tmp.set(0,point.x);
    tmp.set(1,point.y);
    tmp.set(2,point.z);
    tmp.set(3,1);

    Matrix tmp1=completeMatrix*tmp;
    
    Point2D* res=new Point2D(tmp1.get(0, 0)/tmp1.get(0, 3),tmp1.get(0, 1)/tmp1.get(0, 3));
    
    return res;
}

Edge* projectEdge3D(VRC vrc,CononicalVolume canVol,Edge3D e){
    Point2D ini=*projectPoint3D(vrc, canVol, e.ini);
    Point2D fin=*projectPoint3D(vrc, canVol, e.fin);
    Edge *res=new Edge(ini,fin);
    return res;
}

EdgeList* projectEdge3DList(VRC vrc,CononicalVolume canVol,Edge3DList* list3D){
    EdgeList* projEdges=NULL;
    Edge3DList* tmp1=list3D;
    
    while(tmp1!=NULL){
        if(projEdges==NULL){
            projEdges=new EdgeList();
            projEdges->setEdge(*projectEdge3D(vrc, canVol, tmp1->e));
        }
        else{
            projEdges->addEdge(*projectEdge3D(vrc, canVol, tmp1->e));
        }
        tmp1=tmp1->next();
    }

    return projEdges;
}
Edge3DList* getEdge3DListfromPointList(std::list<Point3D> pointList){
    Edge3DList* edgeList=NULL;
    Edge3D* tmp1=NULL;
    std::list<Point3D>::iterator itnext;
    for(std::list<Point3D>::iterator it=pointList.begin();it!=pointList.end();it++){
        
        it++;
        itnext=it;
        it--;
        
        if(itnext==pointList.end()){
            itnext=pointList.begin();
        }
        tmp1=new Edge3D(*it,*itnext);
        if(edgeList==NULL){
            edgeList=new Edge3DList();
            edgeList->setEdge(tmp1->ini.x, tmp1->ini.y, tmp1->ini.z, tmp1->fin.x, tmp1->fin.y, tmp1->fin.z, 1, 0, 0);
        }
        else{
            edgeList->addEdge(*tmp1);
        }
    }
    return edgeList;
}

Edge3DList* discretizeCircle1(float centerX,float centerY,float centerz,bool vertical,float radius,int points){
    std::list<Point3D>Points1;
    float angleDelta=M_PI/points;
    if(vertical){
        for(float angle=0;angle<=2*M_PI;angle+=angleDelta){
            Point3D tmp=*new Point3D((radius*cosf(angle))+centerX,radius*sinf(angle)+centerY,centerz);
            
            Points1.push_back(tmp);
        }
    }else{
        for(float angle=0;angle<=2*M_PI;angle+=angleDelta){
            Point3D tmp=*new Point3D((radius*cosf(angle))+centerX,centerY,radius*sinf(angle)+centerz);
            
            Points1.push_back(tmp);
        }
    }
    
    
    return getEdge3DListfromPointList(Points1);
}



Edge3DList* DiscretizeCylinderBody(float centerX,float centerY,float centerz,bool vertical,float radius,int points,float height){
    Edge3DList* res=NULL;

    
    float angleDelta=M_PI/points;
    if(vertical){
        for(float angle=0;angle<=2*M_PI;angle+=angleDelta){
            Point3D ini=*new Point3D((radius*cosf(angle))+centerX,radius*sinf(angle)+centerY,centerz);
            Point3D fin=*new Point3D((radius*cosf(angle))+centerX,radius*sinf(angle)+centerY,centerz+height);
            
            if(res==NULL){
                res=new Edge3DList(*new Edge3D(ini,fin));
            }
            else{
                res->addEdge(*new Edge3D(ini,fin));
            }
        }
    }else{
        for(float angle=0;angle<=2*M_PI;angle+=angleDelta){
            Point3D ini=*new Point3D((radius*cosf(angle))+centerX,centerY,radius*sinf(angle)+centerz);
            Point3D fin=*new Point3D((radius*cosf(angle))+centerX,centerY+height,radius*sinf(angle)+centerz);
            
            if(res==NULL){
                res=new Edge3DList(*new Edge3D(ini,fin));
            }
            else{
                res->addEdge(*new Edge3D(ini,fin));
            }
        }
    }

    res->merge(discretizeCircle1(centerX, centerY, centerz, vertical, radius, 20));
    if(vertical)
    res->merge(discretizeCircle1(centerX, centerY, centerz+height, vertical, radius, 20));
    else
    res->merge(discretizeCircle1(centerX, centerY+height, centerz, vertical, radius, 20));
    
    return res;
}






//this function takes an input one point which is part of the first 1/8 of circle
// and translate it in the other 8 circle sectors
//it has 5 different mode:
// 0 - draw the full circle
// 1 - draw the half right circle
// 2 - draw the half left circle
// 3 - draw the bottom half circle
// 4 - draw the top half circle
// the different modes are used to shape the name letter.
void draw_CirclePoint(int x, int y,int centerX,int centerY,int mode){
    if(mode==1||mode==0){
    glVertex2f(x+centerX, y+centerY);//UP RIGHT
    glVertex2f(y+centerX, x+centerY);//RIGHT UP
    glVertex2f(y+centerX, -x+centerY);//RIGHT DOWN
    glVertex2f(x+centerX, -y+centerY);//DOWN RIGHT
    }
    if(mode==2||mode==0){
    glVertex2f(-y+centerX, -x+centerY);
    glVertex2f(-x+centerX, y+centerY);
    glVertex2f(-x+centerX, -y+centerY);
    glVertex2f(-y+centerX, x+centerY);
        
    }
    if(mode==3){
        glVertex2f(y+centerX, -x+centerY);//RIGHT DOWN
        glVertex2f(x+centerX, -y+centerY);//DOWN RIGHT
        glVertex2f(-x+centerX, -y+centerY);
        glVertex2f(-y+centerX, -x+centerY);
    }
    
    if(mode==4){
        glVertex2f(-y+centerX, x+centerY);
        glVertex2f(x+centerX, y+centerY);//UP RIGHT
        glVertex2f(y+centerX, x+centerY);//RIGHT UP
        glVertex2f(-x+centerX, y+centerY);
        
    }
    
}

// This function scan convert the first 1/8 of the circle
//then calls the draw_CirclePoints for every point calculated
//which transpose each point in the other sectors of the circle
// the function is invoched by
// draw_Circle_bottom
// draw_Circle_top
// draw_Circle
// draw_Circle_left
// draw_Circle_right
// draw_Circle_colored
// which sets the corresponding mode and color
void draw_Circle_helper(int centerX, int centerY, int radius,int mode,float R, float G, float B){
    int xp=0,yp=radius;
    int d=2*xp-yp-1;
    glColor3f(R,G,B);
    glBegin(GL_POINTS);
    
    draw_CirclePoint(xp, yp,centerX,centerY,mode);
    while(xp<=yp){
        if(d>=0){
            xp++;
            yp--;
            d=d+2*xp-2*yp+5;
            draw_CirclePoint(xp, yp,centerX,centerY,mode);
            
            
        }else{
            xp++;
            d=d+2*xp+3;
            draw_CirclePoint(xp, yp,centerX,centerY,mode);        }
    }
    glEnd();
    glColor3f(1.0, 1.0, 1.0);
    
}

//This function is used by draw_filledCircle
//It fills the circle row by row everytime a new point is found and
//transposed in the other sectors
void draw_filledCirclePoint(int x,int y, int centerX,int centerY,float R, float G, float B){


        fill(y+centerY, -x+centerX, x+centerX, R, G, B);
        fill(x+centerY, -y+centerX, y+centerX, R, G, B);
        fill(-x+centerY, -y+centerX, y+centerX, R, G, B);
        fill(-y+centerY, -x+centerX, x+centerX, R, G, B);
    
    
}

//Like the draw_Circle_helper but calls the draw_filledCirclePoint
//which fills the inside of the circle
void draw_filledCircle(int centerX, int centerY, int radius,float R, float G, float B){
    int xp=0,yp=radius;
    int d=2*xp-yp-1;
    glColor3f(R,G,B);
    glBegin(GL_POINTS);
    
    draw_filledCirclePoint(xp, yp,centerX,centerY,R,G,B);
    while(xp<=yp){
        if(d>=0){
            xp++;
            yp--;
            d=d+2*xp-2*yp+5;
            draw_filledCirclePoint(xp, yp,centerX,centerY,R,G,B);
            
            
        }else{
            xp++;
            d=d+2*xp+3;
            draw_filledCirclePoint(xp, yp,centerX,centerY,R,G,B);        }
    }
    glEnd();
    glColor3f(1.0, 1.0, 1.0);
}


void draw_Circle_bottom(int centerX,int centerY,int radius){
    draw_Circle_helper(centerX, centerY, radius, 3,1.0,0.0,0.0);
}

void draw_Circle_top(int centerX,int centerY,int radius){
    draw_Circle_helper(centerX, centerY, radius, 4,1.0,0.0,0.0);
}

void draw_Circle(int centerX,int centerY,int radius){
    draw_Circle_helper(centerX, centerY, radius, 0,1.0,0.0,0.0);
}

void draw_Circle_colored(int centerX,int centerY,int radius,float R, float G, float B){
    draw_Circle_helper(centerX, centerY, radius, 0,R,G,B);
}

void draw_Circle_right(int centerX,int centerY,int radius){
    draw_Circle_helper(centerX, centerY, radius, 1,1.0,0.0,0.0);
}
void draw_Circle_left(int centerX,int centerY,int radius){
    draw_Circle_helper(centerX, centerY, radius, 2,1.0,0.0,0.0);
}

//Scanconvertion of the line from (x1,y1) to (x2,y2)
// draw the line of the RGB color
// if the x1 point is not the leftmost point swap the two points (x1,y1) and (x2,y2)
// then based on the slope result the algorithm splits in 4 parts
// that assign the initialization value to the decision variable d0
// and sets the increment that X and Y should have if d0 is positive
// or if d0 is negative
// After the variable initialization the same code is run in all four cases
void DrawLine_colored(int x1,int y1,int x2,int y2,float R, float G, float B){
    
    bool switched=false;
    
    if(x1>x2){
        int tmp;
        tmp=x1;
        x1=x2;
        x2=tmp;
        tmp=y1;
        y1=y2;
        y2=tmp;
        switched=true;
    }
    int xp=x1,yp=y1;
    int dx=x2-x1,dy=y2-y1,a=dx,b=-dy,d0=0;
    int incP=0,incN=0,Pnum=0;
    int incXP=0,incXN=0,incYP=0,incYN=0;
    if((float)dy/dx>1){
        d0=2*a+b;
        incP=2*a+2*b;
        incN=2*a;
        Pnum=dy;
        
        incXP=1;
        incXN=0;
        incYP=1;
        incYN=1;
    }
    else if((float)dy/dx>=0){
        d0=a+2*b;
        incP=2*b;
        incN=2*a+2*b;
        Pnum=dx;
        
        incXP=1;
        incXN=1;
        incYP=0;
        incYN=1;
    }
    else if((float)dy/dx>=-1){
        d0=2*b-a;
        incP=2*b-2*a;
        incN=2*b;
        Pnum=dx;
        
        incXP=1;
        incXN=1;
        incYP=-1;
        incYN=0;
    }
    else{
        d0=b-2*a;
        incP=-2*a;
        incN=2*b-2*a;
        Pnum=-dy;
        
        incXP=0;
        incXN=1;
        incYP=-1;
        incYN=-1;
    }
    glColor3f(R,G,B);
    glBegin(GL_POINTS);
    glVertex2f(x1, y1);
    for(int i=0;i<Pnum;i++){
        if(d0<0){
            xp+=incXN;
            yp+=incYN;
            d0=d0+incN;
            glVertex2f(xp, yp);
        }
        else{
            xp+=incXP;
            yp+=incYP;
            d0=d0+incP;
            glVertex2f(xp, yp);
        }
        
    }
    glEnd();
    glColor3f(1.0, 1.0, 1.0);
    
    
    glColor3f(R,G,B);
    glBegin(GL_POINTS);
    if(!switched)
        glVertex2f(x2, y2);
    else
        glVertex2f(x1, y1);
    glEnd();
    glColor3f(1.0, 1.0, 1.0);
    
}
void drawPoligon(std::list<Point2D>points,int r, int g, int b){
    
    std::list<Point2D>::iterator it=points.begin();
    std::list<Point2D>::iterator it_next=it;
    
    for(it_next++;it_next != points.end(); it++,it_next++){
        DrawLine_colored(it->x, it->y, it_next->x, it_next->y, r, g, b);
    }
    DrawLine_colored(points.front().x, points.front().y, points.back().x, points.back().y, r, g, b);
}

// Uses the DrawLine_colored setting the color to red
void DrawLine(int x1,int y1,int x2,int y2){
    DrawLine_colored(x1, y1, x2, y2, 1.0, 0.0, 0.0);
}

void DrawEdge(Edge e){
        DrawLine_colored(e.ini.x, e.ini.y, e.fin.x, e.fin.y, e.color.r, e.color.g, e.color.b);
}
void DrawEdgeList(EdgeList *list){
    EdgeList *listPtr;
    listPtr=list;
    while(listPtr!=NULL){
        DrawEdge(listPtr->e);
        listPtr=listPtr->next();
    }
}
#endif
