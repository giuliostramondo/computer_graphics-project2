//
//  SceneObjects.cpp
//  ComputerGraphics_Project2
//
//  Created by Giulio Stramondo on 29/10/13.
//  Copyright (c) 2013 Giulio Stramondo. All rights reserved.
//

#include "SceneObjects.h"



EdgeObject::EdgeObject(){
    this->edges=new Edge3DList();
    this->initialized=false;
}

void EdgeObject::addEdge3D(Edge3D e){
    if(!this->initialized){
        this->edges->e=e;
        this->initialized=true;
    }
    else{
        this->edges->addEdge(e);
    }
}
void EdgeObject::print(){
    Edge3DList* ptr=this->edges;
    while(ptr!=NULL){
        ptr->e.print();
        ptr=ptr->next();
    }
}

void EdgeObject::translate(int x, int y, int z){
    
}

void EdgeObject::rotate(int axe, float deg){
    
}

Edge3DList* EdgeObject::getEdges(){
    
    return NULL;
}

void EdgeObject::setColor(float r,float g,float b){
    Edge3DList* ptr=this->edges;
    while(ptr!=NULL){
        ptr->e.setColor(r, g, b);
    }
    
}


Cylinder1::Cylinder1(Point3D center,float height,float radius):color(0,0,0),center(0,0,0){
    this->center=center;
    this->height=height;
    this->radius=radius;
    
//    this->edges = discretizeCircle1(this->center.x, this->center.y, this->center.z, true, this->radius, 40);
    
}

void Cylinder1::setColor(float r, float g, float b){
    this->color.r=r;
    this->color.g=g;
    this->color.b=b;
}

void Cylinder1::translate(int x, int y, int z){
    
}
void Cylinder1::rotate(int axe, float deg){
    
}
Edge3DList* Cylinder1::getEdges(){
    return edges;
}
