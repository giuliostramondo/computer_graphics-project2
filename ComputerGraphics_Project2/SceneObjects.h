//
//  SceneObjects.h
//  ComputerGraphics_Project2
//
//  Created by Giulio Stramondo on 29/10/13.
//  Copyright (c) 2013 Giulio Stramondo. All rights reserved.
//

#ifndef ComputerGraphics_Project2_SceneObjects_h
#define ComputerGraphics_Project2_SceneObjects_h
#include "Objects.h"
//#include "DrawFunctions.h"

class WorldObject{
public:
    virtual Edge3DList* getEdges()=0;
    virtual void translate(int x, int y, int z)=0;
    virtual void rotate(int axe,float deg)=0;
    virtual void setColor(float r,float g,float b)=0;
};

class EdgeObject:public WorldObject{
public:
    EdgeObject();
    Edge3DList* edges;
    Edge3DList* getEdges();
    void addEdge3D(Edge3D e);
    void translate(int x, int y, int z);
    void rotate(int axe,float deg);
    void setColor(float r,float g,float b);
    void print();
private:
    bool initialized;
};

class Cylinder1:public WorldObject{
public:
    Cylinder1(Point3D center,float height,float radius);
    Point3D center;
    int height;
    int radius;
    Edge3DList* edges;
    Edge3DList* getEdges();
    void translate(int x, int y, int z);
    void rotate(int axe,float deg);
    void setColor(float r,float g,float b);
    Color1 color;
};

class World{
    std::list<WorldObject> objects;
    Point3D prp;
    Point3D vrp;
    Point3D vup;
    Point3D vpn;
    
    
};

#endif
