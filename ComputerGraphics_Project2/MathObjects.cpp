//
//  MathObjects.cpp
//  ComputerGraphics_Project2
//
//  Created by Giulio Stramondo on 15/10/13.
//  Copyright (c) 2013 Giulio Stramondo. All rights reserved.
//

#include "MathObjects.h"
#include <iostream>
#include <cstdarg>
#include <math.h>

Matrix::Matrix(int size){
    this->size=size;
    this->sizeY=size;
    this->sizeX=size;
    this->squared=true;
    this->items= (float**) malloc(sizeof(float*)*size);
    for(int i=0;i< size;i++){
        float *tmp= (float*) malloc(sizeof(float)*size);
        this->items[i]=tmp;
    }
    this->current_row=0;
}

Matrix::Matrix(int sizeX,int sizeY){
    this->squared=false;
    this->sizeX=sizeX;
    this->sizeY=sizeY;
    this->size=0;
    
    this->items= (float**) malloc(sizeof(float*)*sizeX);
    for(int i=0;i< sizeX;i++){
        float *tmp= (float*) malloc(sizeof(float)*sizeY);
        this->items[i]=tmp;
    }
    this->current_row=0;
}

void Matrix::transpose(){
    float **newItems=(float**) malloc(sizeof(float*)*this->sizeY);
    for(int i=0;i< this->sizeY;i++){
        float *tmp= (float*) malloc(sizeof(float)*this->sizeX);
        newItems[i]=tmp;
    }
    
    for(int i=0;i<this->sizeY;i++)
        for(int k=0;k<this->sizeX;k++){
            newItems[i][k]=this->items[k][i];
        }
    float **toErase=this->items;
    this->items=newItems;
    int tmp=this->sizeX;
    this->sizeX=this->sizeY;
    this->sizeY=tmp;
    
    for(int i=0;i<tmp;i++){
        free(toErase[i]);
    }
    free(toErase);
    
}
void Matrix::set_item(int x, int y, float value){
    Matrix::items[x][y]=value;
}

float Matrix::get(int x, int y){
    return Matrix::items[x][y];
}

void Matrix::set_row(...){
    int tmp;
    int dim;
    va_list args;
    

        dim=this->sizeX;
    
    va_start(args, dim);
    
    for(int i=0; i< dim; i++){
        tmp=va_arg(args, int);
        this->items[i][this->current_row]=tmp;
    }
    va_end(args);
    

        if((this->current_row + 1) < this->sizeY)
            this->current_row++;
        else
            this->current_row=0;
    
    return;
}

void Matrix::set_row_id(int id, float a, float b, float c, float d){
    this->items[0][id]=a;
    this->items[1][id]=b;
    this->items[2][id]=c;
    this->items[3][id]=d;
    return;
}

void Matrix::print(){
    int dimX,dimY;
    
        dimX=this->sizeX;
        dimY=this->sizeY;
    
    for(int i=0;i<dimY;i++){
        for(int k=0;k<dimX;k++){
            std::cout<<this->items[k][i]<<" ";
        }
        std::cout<<"\n";
    }
    std::cout<<"\n";
}

Matrix Matrix::operator*(const Matrix&  other)
{
    float tmp=0;
    //If the two matrix are not moltiplicabile returns null
    if(this->sizeX!=other.sizeY){
        std::cout<<"\nError>> Can't perform dot product on the given matrices\n";
        return NULL;
    }
    
    Matrix result= *new Matrix(other.sizeX,this->sizeY);
    
    for(int i=0;i<result.sizeY;i++)
        for(int k=0;k<result.sizeX;k++){
            
                for(int z=0;z<this->sizeX;z++){
                    tmp+=this->items[z][i]*other.items[k][z];
                }
            result.set_item(k, i, tmp);
            tmp=0;
        }
    
    return result;
}

Matrix Matrix::operator+(const Matrix&  other)
{
    int tmp=0;
    //If the two matrix are not moltiplicabile returns null
    if(this->sizeX!=other.sizeX||this->sizeY!=other.sizeY){
        std::cout<<"\nError>> Can't perform sum on matrices of different sizes\n";
        return NULL;}
    
    Matrix result= *new Matrix(this->sizeX,this->sizeY);
    
    for(int i=0;i<result.sizeY;i++)
        for(int k=0;k<result.sizeX;k++){
            
                tmp=this->items[k][i]+other.items[k][i];
                result.set_item(k, i, tmp);
        }
    
    return result;
}

void Matrix::identity(){
    //If the matrix is not square doesn't do anything
    if(!this->squared){
        std::cout<<"\nError>> Can't make identity matrix on a non square matrix\n";
        return;
    }
    
    for(int i=0;i<this->sizeY;i++)
        for(int k=0;k<this->sizeX;k++){
            if(i==k)this->set_item(k, i, 1);
            else this->set_item(k, i, 0);
            
        }
    return;
}

void Matrix::zeroInit(){
    
    for(int i=0;i<this->sizeY;i++)
        for(int k=0;k<this->sizeX;k++){
            this->set_item(k, i, 0);
        }
    return;
}


Vector::Vector(int size):Matrix(1,size){
    this->size=size;
    this->column=true;
}

float Vector::get(int position){
    if(this->column)
        return this->Matrix::get(0, position);
    else
        return this->Matrix::get(position,0);
}

void Vector::set(int position, float value){
    if(this->column)
        return this->Matrix::set_item(0, position,value);
    else
        return this->Matrix::set_item(position, 0,value);
}
Vector Vector::operator^(const Vector& other){
    if(this->size!=3||other.size!=3)return NULL;
    Vector &f2 = const_cast<Vector&>(other);
    Vector res=*new Vector(3);
    float a=(this->get(1))*(f2.get(2))-(this->get(2))*(f2.get(1));
    float b=(this->get(2))*(f2.get(0))-(this->get(0))*(f2.get(2));
    float c=(this->get(0))*(f2.get(1))-(this->get(1))*(f2.get(0));
    
    res.set(0,a);
    res.set(1,b);
    res.set(2,c);
    return res;
}

float Vector::length(){
    int tmp=0;
    for(int i=0; i<this->size;i++){
        tmp+=(this->get(i))*(this->get(i));
    }
    return sqrtf(tmp);
}

Vector Vector::normalize(){
    Vector res=*new Vector(this->size);
    float lenght=this->length();
    
    for(int i=0;i<this->size;i++){
        res.set(i, this->get(i)/lenght);
    }
    return res;
}

void Vector::transpose(){
    this->Matrix::transpose();
    if(this->column)this->column=false;
    else this->column=true;
}

float Vector::operator*(const Vector &other){
    if(this->size!=other.size)return NULL;
    Matrix res1=NULL;
    
    if(this->column&&!(other.column)){
        Vector &f2 = const_cast<Vector&>(other);
        res1=f2.Matrix::operator*(*this);
        return res1.get(0, 0);
    }
    else if(other.column&&!(this->column)){
        res1=this->Matrix::operator*(other);
        return res1.get(0, 0);
    }
    
    return NULL;
}



