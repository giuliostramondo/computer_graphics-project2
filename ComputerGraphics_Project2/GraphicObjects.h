//
//  GraphicObjects.h
//  ComputerGraphics_Project2
//
//  Created by Giulio Stramondo on 23/10/13.
//  Copyright (c) 2013 Giulio Stramondo. All rights reserved.
//

#ifndef ComputerGraphics_Project2_GraphicObjects_h
#define ComputerGraphics_Project2_GraphicObjects_h

#include "MathObjects.h"
#include "reader.h"




Matrix Rx3D(float angle);
Matrix Ry3D(float angle);
Matrix Rz3D(float angle);
Matrix T3D(float x, float y,float z);
Matrix S3D(float x, float y,float z);
Matrix R(VRC vrc);
Matrix Nper(VRC vrc,CononicalVolume canVol);
Matrix SHper(float SHXpar,float SHYpar);
Matrix Mper(float d);
Matrix Mvv3dv(CononicalVolume canVol);
#endif
