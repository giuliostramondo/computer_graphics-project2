//
//  Objects.cpp
//  Computer_Graphics
//
//  Created by Giulio Stramondo on 23/09/13.
//  Copyright (c) 2013 Giulio Stramondo. All rights reserved.
//

#include "Objects.h"

#include <iostream>
#include "reader.h"
#include "MathObjects.h"
#include "GraphicObjects.h"




Point2D::Point2D(int x, int y){
    this->x=x;
    this->y=y;
}

Point3D::Point3D(float x,float y,float z){
    this->x=x;
    this->y=y;
    this->z=z;
}



Color1::Color1(float r,float g, float b){
      this->r=r;
      this->g=g;
      this->b=b;
 }

Edge::Edge(int x1,int y1, int x2,int y2,float r,float g,float b):ini(x1,y1),fin(x2,y2),color(r,g,b){
}

Edge::Edge(int x1,int y1, int x2,int y2):ini(x1,y1),fin(x2,y2),color(1,0,0){
}


Edge::Edge():ini(0,0),fin(0,0),color(1,0,0){
    
}
Edge::Edge(Point2D ini,Point2D fin):ini(0,0),fin(0,0),color(1,0,0){
    this->ini=ini;
    this->fin=fin;
}
void Edge::print(){
    std::cout<<"Points: ("<<this->ini.x<<","<<this->ini.y<<") ("<<this->fin.x<<","<<this->fin.y<<")\nColor: ("<<this->color.r<<","<<this->color.g<<","<<this->color.b<<")\n\n";
    return;
}
void Edge::setColor(float r,float g,float b){
    this->color.r=r;
    this->color.g=g;
    this->color.b=b;
}

Edge3D::Edge3D():ini(0,0,0),fin(0,0,0),color(0,0,0){
    
}
Edge3D::Edge3D(Point3D ini,Point3D fin):ini(0,0,0),fin(0,0,0),color(0,0,0){
    this->ini.x=ini.x;
    this->ini.y=ini.y;
    this->ini.z=ini.z;
    
    this->fin.x=fin.x;
    this->fin.y=fin.y;
    this->fin.z=fin.z;
}
void Edge3D::setColor(float r, float g, float b){
    this->color.r=r;
    this->color.g=g;
    this->color.b=b;
}

void Edge3D::print(){
std::cout<<"Points: ("<<this->ini.x<<","<<this->ini.y<<","<<this->ini.z<<") ("<<this->fin.x<<","<<this->fin.y<<","<<this->fin.z<<")\nColor: ("<<this->color.r<<","<<this->color.g<<","<<this->color.b<<")\n\n";
    return;
}
EdgeList::EdgeList(){
    this->succ=0;
    this->h=this;
}

EdgeList::EdgeList(Edge e){
    this->succ=0;
    this->e=e;
    this->h=this;
}

void EdgeList::addEdge(Edge e){
    EdgeList *tmp=this;
    while(tmp->succ!=0)tmp=tmp->succ;
    EdgeList *newNode=new EdgeList(e);
    newNode->h=this->h;
    tmp->succ=newNode;
    return;
}
void EdgeList::setEdge(int x1,int y1, int x2,int y2,float r,float g,float b){
    this->e=*new Edge(x1,y1,x2,y2,r,g,b);
    return;
}

void EdgeList::setEdge(Edge e){
    this->e.ini=e.ini;
    this->e.fin=e.fin;
}

EdgeList* EdgeList::next(){
    return this->succ;
}

EdgeList* EdgeList::head(){
    return this->h;
}

void EdgeList::print(){
    EdgeList* tmp=this;
    while(tmp!=NULL){
        tmp->e.print();
        tmp=tmp->next();
    }
}
void EdgeList::setColor(float r,float g,float b){
    EdgeList* ptr=this;
    while(ptr!=NULL){
    ptr->e.color.r=r;
    ptr->e.color.g=g;
    ptr->e.color.b=b;
    ptr=ptr->next();
    }
}
Edge3DList::Edge3DList(){
    this->succ=0;
    this->h=this;

}
Edge3DList::Edge3DList(Edge3D e){
        this->e.ini.x=e.ini.x;
        this->e.ini.y=e.ini.y;
        this->e.ini.z=e.ini.z;
    
        this->e.fin.x=e.fin.x;
        this->e.fin.y=e.fin.y;
        this->e.fin.z=e.fin.z;
    
        this->e.color.r=e.color.r;
        this->e.color.g=e.color.g;
        this->e.color.b=e.color.b;
    
    this->succ=0;
    this->h=this;

}
void Edge3DList::setEdge(float x1,float y1,float z1, float x2,float y2,float z2, float r,float g,float b){
    this->e.ini.x=x1;
    this->e.ini.y=y1;
    this->e.ini.z=z1;
    this->e.fin.x=x2;
    this->e.fin.y=y2;
    this->e.fin.z=z2;
    this->e.color.r=r;
    this->e.color.g=g;
    this->e.color.b=b;
    return;
}

void Edge3DList::addEdge(Edge3D e){
    Edge3DList *ptr=this;
    
    while(ptr->next()!=NULL) ptr=ptr->next();
    

    Edge3DList* newEdge=new Edge3DList(e);
    newEdge->h=this->h;
    ptr->succ=newEdge;
    return;
}

void Edge3DList::merge(Edge3DList* toAppend){
    Edge3DList* ptr=this;
    while(ptr->next()!=NULL)ptr=ptr->next();
    ptr->succ=toAppend;
}

Edge3DList* Edge3DList::next(){
    return this->succ;
}

Edge3DList* Edge3DList::head(){
    return this->h;
}

void Edge3DList::print(){
    Edge3DList *tmp=this;
    while(tmp!=NULL){
        tmp->e.print();
        tmp=tmp->next();
    }
}
