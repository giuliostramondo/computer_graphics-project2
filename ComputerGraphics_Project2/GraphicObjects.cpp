//
//  GraphicObjects.cpp
//  ComputerGraphics_Project2
//
//  Created by Giulio Stramondo on 23/10/13.
//  Copyright (c) 2013 Giulio Stramondo. All rights reserved.
//

#include "GraphicObjects.h"
#include <math.h>

Matrix Rx3D(float rad){
    Matrix res= *new Matrix(4);
    
    float cosA=cosf(rad);
    float sinA=cosf(rad);
    
    res.set_row_id(0,1,  0 ,   0 ,0);
    res.set_row_id(1,0,cosA,-sinA,0);
    res.set_row_id(2,0,sinA, cosA,0);
    res.set_row_id(3,0,  0 ,   0 ,1);
    
    return res;
}

Matrix Ry3D(float rad){
    Matrix res= *new Matrix(4);
    
    float cosA=cosf(rad);
    float sinA=cosf(rad);
    
    res.set_row_id(0,cosA ,0,sinA,0);
    res.set_row_id(1,  0  ,1,  0 ,0);
    res.set_row_id(2,-sinA,0,cosA,0);
    res.set_row_id(3,0,  0 ,   0 ,1);
    
    return res;
}

Matrix Rz3D(float rad){
    Matrix res= *new Matrix(4);
    
    float cosA=cosf(rad);
    float sinA=cosf(rad);
    
    res.set_row_id(0,cosA,-sinA,0,0);
    res.set_row_id(1,sinA, cosA,0,0);
    res.set_row_id(2,  0 ,   0 ,1,0);
    res.set_row_id(3,  0 ,   0 ,0,1);
    
    return res;
}

Matrix T3D(float x, float y,float z){
    Matrix res= *new Matrix(4);
    
    res.set_row_id(0,1,0,0,x);
    res.set_row_id(1,0,1,0,y);
    res.set_row_id(2,0,0,1,z);
    res.set_row_id(3,0,0,0,1);
    
    return res;
}

Matrix S3D(float x, float y,float z){
    Matrix res= *new Matrix(4);
    
    res.set_row_id(0,x,0,0,0);
    res.set_row_id(1,0,y,0,0);
    res.set_row_id(2,0,0,z,0);
    res.set_row_id(3,0,0,0,1);
    
    return res;
}

Matrix SHper(float SHXper,float SHYper){
    Matrix res = *new Matrix(4);
    res.set_row_id(0,1,0,SHXper,0);
    res.set_row_id(1,0,1,SHYper,0);
    res.set_row_id(2,0,0,1,0);
    res.set_row_id(3,0,0,0,1);
    return res;
}

Vector DOP(CononicalVolume canVol, VRC vrc){
    Vector dop=*new Vector(4);
    float CWx=(canVol.uMax+canVol.uMin)/2;
    float CWy=(canVol.vMax+canVol.vMin)/2;
    dop.set(0, CWx-vrc.prp.x);
    dop.set(1, CWy-vrc.prp.y);
    dop.set(2, -vrc.prp.z);
    dop.set(3, 0);
    
    return dop;
}
Matrix R(VRC vrc){
    
    Vector Rz=*new Vector(3);
    Rz.set(0, vrc.vpn.x);
    Rz.set(1, vrc.vpn.y);
    Rz.set(2, vrc.vpn.z);

    Rz.normalize();

    
    Vector V_UP=*new Vector(3);
    V_UP.set(0, vrc.vup.x);
    V_UP.set(1, vrc.vup.y);
    V_UP.set(2, vrc.vup.z);
 

    Vector Rx=V_UP^Rz;
    Rx.normalize();

    
    Vector Ry=Rz^Rx;


    Matrix res = *new Matrix(4);
    res.set_row_id(0,Rx.get(0),Rx.get(1),Rx.get(2),0);
    res.set_row_id(1,Ry.get(0),Ry.get(1),Ry.get(2),0);
    res.set_row_id(2,Rz.get(0),Rz.get(1),Rz.get(2),0);
    res.set_row_id(3,0,0,0,1);
    

    return res;
    
}
Matrix Nper(VRC vrc,CononicalVolume canVol){
    

    Vector dop=DOP(canVol,vrc);
    float Sper_x=2*(-vrc.prp.z)/((canVol.uMax-canVol.uMin)*((-vrc.prp.z)+canVol.zMax));
    float Sper_y=2*(-vrc.prp.z)/((canVol.vMax-canVol.vMin)*((-vrc.prp.z)+canVol.zMax));
    float Sper_z=-1/((-vrc.prp.z)+canVol.zMax);
    

    
    Matrix res=(S3D(Sper_x, Sper_y, Sper_z)*(SHper(-(dop.get(0)/dop.get(2)), -(dop.get(1)/dop.get(2)))*(T3D(-vrc.prp.x, -vrc.prp.y, -vrc.prp.z)*(R(vrc)*T3D(-vrc.vrp.x, -vrc.vrp.y, -vrc.vrp.z)))));

    
    return res;
}

Matrix Mper(float d){
    Matrix res=*new Matrix(4);
    
    res.set_row_id(0,1,0,0,0);
    res.set_row_id(1,0,1,0,0);
    res.set_row_id(2,0,0,0,0);
    res.set_row_id(3,0,0,1/d,1);
    
    return res;
}

Matrix Mvv3dv(CononicalVolume canVol){
    float Sx=1.2*800/2;
    float Sy=800/2;
    float Sz=800;
    
    Matrix res= S3D(Sx, Sy, Sz)*T3D(1, 1, 1);
    
    return res;

}
